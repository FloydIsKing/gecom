import { Component } from '@angular/core';

/**
 * Controller that handels the faq page
 * 
 * @author Floyd Kissoon
 * @since 20180330
 */
@Component({
    selector: "faq",
    templateUrl: "./faq.html"
})
export class FAQComponent
{}