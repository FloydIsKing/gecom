import { Component } from '@angular/core';

/**
 * Controller that handels the national Reaistration page
 * 
 * @author Floyd Kissoon
 * @since 20180330
 */
@Component({
    selector: "national-registration",
    templateUrl: "./national-registration.html"
})
export class NationalRegistrationComponent
{}