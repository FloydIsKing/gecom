import { Component } from '@angular/core';

/**
 * Controller that handels the reports page
 * 
 * @author Floyd Kissoon
 * @since 20180330
 */
@Component({
    selector: "reports-and-manuals",
    templateUrl: "./reports-and-manuals.html"
})
export class ReportsAndManualsComponent
{}