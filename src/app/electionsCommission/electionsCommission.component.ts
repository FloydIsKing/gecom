import { Component } from '@angular/core';


/**
 * Controller that handels the elections commission page
 * 
 * @author Floyd Kissoon
 * @since 20180327
 */
@Component({
    selector: "election-commission-about",
    templateUrl: "./elections-commission.html"
})
export class ElectionsCommissionComponent
{}