import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';

import {AboutComponent} from './about/about.component';
import {AboutGuyanaComponent} from './about/aboutGuyana.component';
import {ElectionsCommissionComponent} from './electionsCommission/electionsCommission.component';
import {ElectionsInGuyanaOverviewComponent} from './electionsInGuyana/overview.component';
import {LocalGovernmentComponent} from './localGovernment/localGovernment.component';
import {NationalRegistrationComponent} from './nationalRegistration/nationRegistration.component';
import {CRegistrationComponent} from './nationalRegistration/cRegstration.component';
import {ReportsAndManualsComponent} from './reportsAndManuals/reportsAndManuals.component';
import {MediaReleasesComponent} from './mediaReleases/mediaReleases.component';
import {FAQComponent} from './faq/faq.component';
import {VacanciesAndNoticesComponent} from './vacanciesAndNotices/vacanciesAndNotices.component';
import {MapOfMunicipalitiesComponent} from './mapOfMunicipalities/mapOfMunicipalities.component';
import {LinksComponent} from './links/links.component';
import {ElectionsInGuyanaConstitutionReformComponent} from './electionsInGuyana/constitutionReform.component';
import {ContactComponent} from './contact/contact.component';

@NgModule({
    declarations: [
        AppComponent,
        AboutComponent, AboutGuyanaComponent,
        ElectionsCommissionComponent,
        ElectionsInGuyanaOverviewComponent, ElectionsInGuyanaConstitutionReformComponent,
        LocalGovernmentComponent,
        NationalRegistrationComponent,
        CRegistrationComponent,
        ReportsAndManualsComponent,
        FAQComponent,
        MediaReleasesComponent,
        VacanciesAndNoticesComponent,
        MapOfMunicipalitiesComponent,
        LinksComponent,
        ContactComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
