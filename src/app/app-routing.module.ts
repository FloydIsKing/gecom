import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AboutComponent } from './about/about.component';
import { ElectionsCommissionComponent } from './electionsCommission/electionsCommission.component';
import {ElectionsInGuyanaOverviewComponent} from './electionsInGuyana/overview.component';
import {LocalGovernmentComponent} from './localGovernment/localGovernment.component';
import {NationalRegistrationComponent} from './nationalRegistration/nationRegistration.component';
import {CRegistrationComponent} from './nationalRegistration/cRegstration.component';
import {ReportsAndManualsComponent} from './reportsAndManuals/reportsAndManuals.component';
import {MediaReleasesComponent} from './mediaReleases/mediaReleases.component';
import {FAQComponent} from './faq/faq.component';
import {VacanciesAndNoticesComponent} from './vacanciesAndNotices/vacanciesAndNotices.component';
import {MapOfMunicipalitiesComponent} from './mapOfMunicipalities/mapOfMunicipalities.component';
import {LinksComponent} from './links/links.component';
import { AboutGuyanaComponent } from './about/aboutGuyana.component';
import {ElectionsInGuyanaConstitutionReformComponent} from './electionsInGuyana/constitutionReform.component';
import {ContactComponent} from './contact/contact.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: AboutComponent },
  { path: 'elections-commission', component: ElectionsCommissionComponent },
  { path: 'elections-in-guyana/overview', component: ElectionsInGuyanaOverviewComponent },
  { path: 'local-government', component: LocalGovernmentComponent },
  { path: 'national-registration', component: NationalRegistrationComponent },
  { path: 'c-registration', component: CRegistrationComponent },
  { path: 'reports-and-manuals', component: ReportsAndManualsComponent },
  { path: 'media-releases', component: MediaReleasesComponent },
  { path: 'faq', component: FAQComponent },
  { path: 'vacancies-and-notice', component: VacanciesAndNoticesComponent },
  { path: 'maps-of-municipalities', component: MapOfMunicipalitiesComponent },
  { path: 'links', component: LinksComponent },
  { path: 'about-guyana', component: AboutGuyanaComponent },
  { path: 'contact-us', component: ContactComponent },
  { path: 'elections-in-guyana/constitution-reform', component: ElectionsInGuyanaConstitutionReformComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}