import { Component } from '@angular/core';


/**
 * Controller that handels the local government page
 * 
 * @author Floyd Kissoon
 * @since 20180330
 */
@Component({
    selector: "local-government",
    templateUrl: "./local-government.html"
})
export class LocalGovernmentComponent
{}