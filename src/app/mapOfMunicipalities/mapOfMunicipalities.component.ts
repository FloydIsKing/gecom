import { Component } from '@angular/core';

/**
 * Controller that handels the vacancies and notice page
 * 
 * @author Floyd Kissoon
 * @since 20180330
 */
@Component({
    selector: "map-of-municipalities",
    templateUrl: "./map-of-municipalities.html"
})
export class MapOfMunicipalitiesComponent
{}