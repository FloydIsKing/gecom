import { Component } from '@angular/core';

/**
 * Controller that handels the contact page page
 * 
 * @author Floyd Kissoon
 * @since 20180330
 */
@Component({
    selector: "contact",
    templateUrl: "./contact.html"
})
export class ContactComponent
{}