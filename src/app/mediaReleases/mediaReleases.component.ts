import { Component } from '@angular/core';

/**
 * Controller that handels the media releases page
 * 
 * @author Floyd Kissoon
 * @since 20180330
 */
@Component({
    selector: "media-releases",
    templateUrl: "./media-releases.html"
})
export class MediaReleasesComponent
{}