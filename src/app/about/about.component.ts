import { Component } from '@angular/core';


/**
 * Controller that handels the about page
 * 
 * @author Floyd Kissoon
 * @since 20180327
 */
@Component({
    selector: "gecome-about",
    templateUrl: "./about.html"
})
export class AboutComponent
{}