import { Component } from '@angular/core';

/**
 * Controller that handels the vacancies and notice page
 * 
 * @author Floyd Kissoon
 * @since 20180330
 */
@Component({
    selector: "gecom-links",
    templateUrl: "./links.html"
})
export class LinksComponent
{}