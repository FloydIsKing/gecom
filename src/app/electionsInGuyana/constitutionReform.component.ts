import { Component } from '@angular/core';


/**
 * Controller that handels the consitutional reform page
 * 
 * @author Floyd Kissoon
 * @since 20180330
 */
@Component({
    selector: "election-in-guyana-constitution-reform",
    templateUrl: "./constitution-reform.html"
})
export class ElectionsInGuyanaConstitutionReformComponent
{}