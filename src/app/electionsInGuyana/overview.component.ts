import { Component } from '@angular/core';


/**
 * Controller that handels the overview page
 * 
 * @author Floyd Kissoon
 * @since 20180330
 */
@Component({
    selector: "election-in-guyana-overview",
    templateUrl: "./overview.html"
})
export class ElectionsInGuyanaOverviewComponent
{}